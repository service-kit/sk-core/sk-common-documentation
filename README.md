# sk-common-documentation

| Build Status | Coverage Report |
| :----------- | :-------------- |
| [![pipeline status](https://gitlab.com/service-kit/sk-core/sk-common-documentation/badges/main/pipeline.svg)](https://gitlab.com/service-kit/sk-core/sk-common-documentation/-/commits/main) | [![coverage report](https://gitlab.com/service-kit/sk-core/sk-common-documentation/badges/main/coverage.svg)](https://gitlab.com/service-kit/sk-core/sk-common-documentation/-/commits/main) |

See the [published documentation](https://servicekit.bluenova.software/) for details.
