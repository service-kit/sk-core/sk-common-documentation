---
id: 4xeqov7xpx8qctfi939zdj8
title: BuildKit
desc: ''
updated: 1650815458034
created: 1650814550289
---

Service Kit makes use of [BuildKit](https://docs.docker.com/develop/develop-images/build_enhancements/) in order to build Docker images for multiple target architecture platforms. ^buildkit-overview-1

Multiple platforms can be specified via the `DOCKER_PLATFORMS` variable. Buildkit will build layers for each platform such that the image can be run on each of those platforms. By default service kit is configured to build for `amd64` and `arm64`. ^buildkit-overview-2

## BuildKit and Custom Registry CA's

It is common to require pushing Docker images to a private Docker registry that is configured with a custom private certificate authority (CA).  
The Docker build context for a BuildKit build executes within a Docker container itself. This means that when the Docker push is executed at the end of the build, the CA must be trusted by the BuildKit builder. This requires building a custom builder image which has the CA copied and trusted within.

See the [[projects.sk-tools-buildkit]] project for information on building a custom builder image.
