---
id: ssfe3qkanf6ewd4qaiotp5k
title: Pass
desc: ''
updated: 1650819281021
created: 1650818729560
---

Several of the Service Kit utility scripts make use of [Pass: the standard unix password manager](https://www.passwordstore.org/).

See the above link for official documentation on how to install and configure `pass`.

*Note*: The official documentation uses a string as the argument for `pass init` however it has been noted that `pass` actually expects the ID of a GPG key to be passed in.

```bash
gpg --full-generate-key

gpg -K # list keys

# get the key id e.g., # 1DA8F93F8942F0196F1AC07BCDC4D76E9CDDDDDD

pass init 1DA8F93F8942F0196F1AC07BCDC4D76E9CDDDDDD
```

Once the password store has been initialzed it is possible to create new secret entries with the `pass insert` command.

`pass` is primarily used to store and access passwords for Helm repositories.

```bash
$ pass insert my-helm-repo-password
...
```

The password can then be accessed with

```bash
pass my-helm-repo-password
```

Upon requesting access to a stored secret, the user is prompted to enter the password to unlock the GPG keychain. Once unlocked, the user will not be prompted again for several minutes.
