---
id: gk8k1epls717bvh8r67j437
title: Service Kit Key Generator
desc: ''
updated: 1650923286924
created: 1650922913810
---

The Key Generator is a tool which generates a Django compatible secret key. The tool creates a Kubernetes secret containing the key; which can then be loaded and use by Django based applications.  
Further documentation is available [here](https://gitlab.com/service-kit/sk-core/sk-key-generator). ^sk-key-generator-overview