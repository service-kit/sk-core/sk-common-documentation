---
id: 83yzr2xxscijot381do0lem
title: Service Kit Kube Toolset
desc: ''
updated: 1651011191133
created: 1650820597187
---

The Service Kit Kube Toolset is a Helm chart that deploys several supporting functions required by Service Kit projects. ^sk-kube-toolset-overview

These supporting functions include:

* [Nginx Ingress Controller](https://github.com/nginxinc/kubernetes-ingress)
* [cert-manager](https://cert-manager.io/docs/)
* [Zalando postgres-operator](https://github.com/zalando/postgres-operator)
* [pgadmin4](https://gitlab.com/bluenovasoftware/pgadmin4-helm)
* [Grafana](https://grafana.com/)
* [Loki](https://grafana.com/oss/loki/)

The Kube toolset chart can be deployed as follows:

```bash
$ helm -n sk-system upgrade --install sk-system servicekit/sk-kube-toolset --version 1.0.0 --set-file tls.localCA.crt=../sk-tools-ca/servicekit-root.crt --set-file tls.localCA.key=../sk-tools-ca/servicekit-root.key
```

The CA file paths should map to a certificate authority used by cert-manager.

See [[projects.sk-tools-ca]] for details on creating a private certificate authority.

While default, the cert-manager CA is optional and can be disabled via Helm values.
