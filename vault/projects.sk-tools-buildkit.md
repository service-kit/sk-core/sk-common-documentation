---
id: 61hd63ihurqfsf5nkf1famd
title: Service Kit Tools Buildkit
desc: ''
updated: 1650816650307
created: 1650815325922
config: { global: { enablePrettyRefs: false } }
---

![[concepts.buildkit#^buildkit-overview-1]]
![[concepts.buildkit#^buildkit-overview-2]]

## Building a BuildKit builder with a CA

The [Tools BuildKit](https://gitlab.com/service-kit/sk-core/sk-tools-buildkit/-/tree/main/) serves as a utility project for the creation of a custom BuildKit builder image that contains a custom Certificate Authority.

Note that the project does not create the certificate authority. It is expected that the certificate files already exist and can be specified via the `CERTS` variable.

### Makefile Variables

The Makefile exposes the following variables.

Variable | Note
---------|-----
IMAGE_BASE | The Docker image to use as a base. This should be `moby/buildkit:buildx-stable-1`
TARGET_IMAGE | The name of the target image. 
CERTS | The space separated list of certificate files to be copied into the image.
PLATFORM | The target Buildkit platforms for which to build images. Defaults to `linux/amd64,linux/arm64`.

### Makefile Targets

Target | Note
-------|-----
fetch-certs | Copy the `CERTS` files to the local directory. See [[projects.sk-tools-ca]] for steps to generate a certificate authority.
docker-build | Build the builder Docker image containing the certificate authority.
install-binfmt | Install the `binfmt` binary formats required by BuildKit.
create-builder | Create the BuildKit builder instance. The name of the builder should be set as the `DOCKER_BUILDKIT_BUILDER` Makefile variable for base projects.

