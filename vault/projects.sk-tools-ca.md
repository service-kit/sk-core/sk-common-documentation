---
id: ijf60f5gsbuwr64nwcy8ow9
title: Service Kit Tools CA
desc: ''
updated: 1650821502653
created: 1650816191997
---

The [Service Kit Tools CA project](https://gitlab.com/service-kit/sk-core/sk-tools-ca) contains a single script that serves as an example for generating a private certificate authority.  
It is intended to be used with the [[projects.sk-tools-buildkit]] project.

```bash
openssl genrsa -out servicekit-root.key 2048
openssl req -x509 -new -nodes -key servicekit-root.key -sha256 -days 1825 -out servicekit-root.crt
```
