---
id: 27ybe2ak1kv2h1hdid3n8hu
title: Service Kit Tools Kube Prometheus
desc: ''
updated: 1651097658040
created: 1651095160926
---

The Kube Prometheus tools project is a fork of [kube-prometheus](https://github.com/prometheus-operator/kube-prometheus/). The primary difference in the fork is the addition of Service Kit specific configuration. The deployment and management of Grafana is removed from Kube Prometheus as it is managed by the Service Kit Kubernetes Toolset. The official Helm chart for Grafana supports enabling the dashboard loading sidecar; which is not currently available in the Grafana managed by Kube Prometheus. ^sk-tools-kube-prometheus-overview

Build and deploy the project by executing the following Makefile targets:

1. `make jb-init-kube-prom`   
Clone the official Kube Prometheus jsonnet library.
2. `make jb-update-kube-prom`  
Pull updates from the remote jsonnet library. Not needed directly after an `init`.
3. `make build-kube-prom`  
Build manifests based on configuration.
4. `make apply`
`kubectl`  
Apply all generated manifest files.
