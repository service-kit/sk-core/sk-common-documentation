---
id: nivf1ih2gah9n48gfq08jrf
title: Service Kit
desc: ''
updated: 1651095289138
created: 1649283498085
---

Service Kit is a collection of projects which serve as a toolchain for developing cloud-native microservices. The aim is to provide a project starting that has preconfigured functionality not directly related to the business logic of the service. Having this functionality available out-of-the-box allows developerS to focus on building their API without needing to implement features common to web service projects. ^sk-overview

Service Kit and its associated projects are published under the [MIT Open Source License](https://opensource.org/licenses/MIT). ^sk-license-notice

## Base Projects

At its core, Service Kit microservices are built upon existing frameworks sudo as [Django Rest Framework](https://www.django-rest-framework.org/) for Python or [ExpressJS](https://expressjs.com/) for NodeJS. The base projects build upon these frameworks to provide and integrate with the following.

1. The foundation for a microservice project i.e., DRF or Express. Sample models and APIs pre-defined.
2. Unit test tooling and examples.
3. Database integration. Connectivity and ORM to a supported database backends. Helm chart includes the deployment of the databse.
4. Automated build process.
5. Containerized packaging. Docker image build for the service and Helm chart for deploying to Kubernetes. Docker Buildkit enables support for multi-architecture images.
6. Smoke and load testing tools ([k6](https://k6.io/)) with examples.
7. Gitlab based CI/CD pipelines*.
8. Observability*; Prometheus exports and Grafana dashboards.
9. Authentication*; OIDC based RBAC.
10. Documentation tooling
    * Detailed documentation of the Service Kit project and its components.
    * Tooling ([Dendron](https://dendron.so)) for writing and publishing documentation of projects built upon Service Kit.

\* Not yet implemented but will be delievered in 1.0.0.

### sk-base-py

`sk-base-py` is the first implementation of a Service Kit base project. It is a Python implementation which provides functionality listed above on top Python tools including the [Django Rest Framework](https://www.django-rest-framework.org/), [pyscopg2](https://www.psycopg.org/), and [pytest](https://docs.pytest.org/). ^sk-basepy-overview

Further documentation is available [here](https://gitlab.com/service-kit/sk-base/sk-base-py).

## Utility Projects

Service Kit includes several utility projects that aid in the building and deploying of cloud-native microservices.

### sk-kube-toolset

The Service Kit Kubernetes Toolset is a Helm chart that bundles various third-party tools for monitoring, database operations, certificate management, and more. 

Further documentation is available [here](https://gitlab.com/service-kit/sk-core/sk-kube-toolset).

### sk-tools-kube-prometheus

![[projects.sk-tools-kube-prometheus#^sk-tools-kube-prometheus-overview]]

Unlike other Service Kit projects, the Kube Prometheus fork maintains the APACHE 2.0 license under which the original project is distributed.

### sk-key-generator

![[projects.sk-key-generator#^sk-key-generator-overview]]

### sk-tools-ca

The Service Kit CA Tools project is a simple shell script that generates a root certificate authority (CA) for use in local development environments.

Further documentation is available [here](https://gitlab.com/service-kit/sk-core/sk-tools-ca).

### sk-tools-buildkit

Docker images for Service Kit projects are built using the [Docker Buildx](https://docs.docker.com/buildx/working-with-buildx/) extension. Buildx contexts are hosted in Docker containers. Trusted CAs on the host machine are not available within the Buildx container and thus are not available when pushing Docker images to a registry with TLS.  
The Service Kit Buildkit Tools project builds a custom Buildkit `builder` image which contains a custom CA; enabling Buildkit containers to push to registries secured with TLS certificates signed by that CA.

Further documentation is available [here](https://gitlab.com/service-kit/sk-core/sk-tools-buildkit).
